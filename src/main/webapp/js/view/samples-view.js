define([ 'backbone', 'resthub', 'collection/samples', 'hbs!template/samples' ],
    function (Backbone, Resthub, Samples, samplesTemplate) {

        return Resthub.View.extend({

            // Define view template
            template: samplesTemplate,


            events: {
                'keyup #inp-filter': 'doFilter'
            },

            initialize: function () {
                // Initialize the collection
                this.collection = new Samples();

                // Render the view when the collection is retreived from the server
                this.listenTo(this.collection, 'sync', this.render2);

                // Request unpaginated URL
                this.collection.fetch({ data: { page: 'no'} });
            },

            doFilter: function () {
                this.collection.fetch(
                    {
                        data: $.param(
                            {
                                name: this.$('#inp-filter').val()
                            })
                    });
            },

            render2: function () {
                var val1 = this.$('#inp-filter').val();
                this.render();
                this.$('#inp-filter').val(val1);
                this.$('#inp-filter').focus();
            }

        });
    });