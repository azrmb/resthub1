define(['backbone', 'view/about-view', 'view/samples-view'],
    function (Backbone, AboutView, SamplesView) {

        return Backbone.Router.extend({

            initialize: function () {
                Backbone.history.start({ pushState: true, root: "/test1/" });
            },

            routes: {
                '': 'home',
                'home': 'home',
                'about': 'about',
                'todo': 'todo'
            },

            home: function () {
                new SamplesView({ root: $('#main') });
            },
            about: function () {
                new AboutView({ root: $('#main') });
            },
            todo: function () {
            }

        });

    });