define(['backbone', 'model/sample'], function (Backbone, Sample) {

    var Samples = Backbone.Collection.extend({

        // Reference to this collection's model.
        model: Sample,
        url: 'api/sample',

        byName: function (name) {
            var filtered = this.filter(function (sample) {
                return sample.get("name").match(new RegExp('.*' + name + '.*')) != null;
            });
            return new Samples(filtered);
        }

    });
    return Samples;
});
