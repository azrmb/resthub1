package grp1;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConfigurationBean {

    @Value(value = "${testValue:valuenotfound}")
    public String testValue;

}
