package grp1.tests;


import grp1.model.Sample;
import grp1.repository.SampleRepository;
import org.fest.assertions.api.Assertions;
import org.resthub.test.AbstractTransactionalTest;
import org.springframework.test.context.ActiveProfiles;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import javax.inject.Inject;

@ActiveProfiles({"resthub-jpa", "resthub-pool-bonecp"})
public class FirstTransactionalTest extends AbstractTransactionalTest {

    private SampleRepository sampleRepository;

    @Inject
    public void setSampleRepository(SampleRepository sampleRepository) {
        this.sampleRepository = sampleRepository;
    }

    @AfterTest
    public void tearDown() {
//        for (Sample sample : sampleRepository.findByName()) {
//            sampleRepository.delete(sample);
//        }
    }

    @Test
    public void testInsert() {
        Sample sample = sampleRepository.save(new Sample("for example test1"));
        Assertions.assertThat(sampleRepository.exists(sample.getId())).isTrue();
    }
}
