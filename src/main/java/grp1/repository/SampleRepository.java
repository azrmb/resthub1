package grp1.repository;

import grp1.model.Sample;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface SampleRepository extends JpaRepository<Sample, Long> {

    @Query("FROM Sample WHERE name LIKE %?1%")
    public List<Sample> findByNameLike(String name);

}
