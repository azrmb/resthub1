package grp1.controller;

import grp1.model.Sample;
import grp1.repository.SampleRepository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.List;

@RestController
public class Controller1 {

    private SampleRepository sampleRepository;

    @Inject
    public void setSampleRepository(SampleRepository sampleRepository) {
        this.sampleRepository = sampleRepository;
    }

    @RequestMapping("/c1")
    public String getText(@RequestParam(value = "f") String f) {

        List<Sample> samples = sampleRepository.findByNameLike(f);
        if (samples.size() == 0) {
            return "There are no suitable samples.";
        } else {
            StringBuilder sb = new StringBuilder();
            for (Sample sample : samples) {
                sb.append(sample.getName() + "<br/>");
            }
            return String.valueOf(sb.toString());
        }


    }
}
