package grp1.notApp;


import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;

public class TestMapping {

    public static void main(String... args) {
        Person person1 = new Person("Anna", "Petrovicha", 30, new Address("Brest", "Moskovskaya", "300"), null);
        Person person2 = new Person("Henry", "Petrovich", 30, new Address("Brest", "Moskovskaya", "300"), person1);
        System.out.println(person2);

        ModelMapper modelMapper = new ModelMapper();
        PropertyMap<Person, PersonDTO> propertyMap = new PropertyMap<Person, PersonDTO>() {
            @Override
            protected void configure() {
                map().setAddressCity("asdf");
            }
        };
//        modelMapper.addMappings(propertyMap);
//        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        PersonDTO dto = modelMapper.map(person2, PersonDTO.class);
        System.out.println(dto);
    }

    static class Person {
        private String firstName;
        private String lastName;
        private int age;
        private Address address;
        private Person spouse;

        Person(String firstName, String lastName, int age, Address address, Person spouse) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.age = age;
            this.address = address;
            this.spouse = spouse;
        }

        public Person getSpouse() {
            return spouse;
        }

        public void setSpouse(Person spouse) {
            this.spouse = spouse;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        public Address getAddress() {
            return address;
        }

        public void setAddress(Address address) {
            this.address = address;
        }

        @Override
        public String toString() {
            return "Person{" +
                    "firstName='" + firstName + '\'' +
                    ", lastName='" + lastName + '\'' +
                    ", age=" + age +
                    ", addressCity=" + address +
                    ", spouse=" + spouse +
                    '}';
        }
    }

    static class Address {
        private String city;
        private String street;
        private String building;

        Address(String city, String street, String building) {
            this.city = city;
            this.street = street;
            this.building = building;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getStreet() {
            return street;
        }

        public void setStreet(String street) {
            this.street = street;
        }

        public String getBuilding() {
            return building;
        }

        public void setBuilding(String building) {
            this.building = building;
        }

        @Override
        public String toString() {
            return "Address{" +
                    "city='" + city + '\'' +
                    ", street='" + street + '\'' +
                    ", building='" + building + '\'' +
                    '}';
        }
    }

    static class PersonDTO {
        private String firstName;
        private String lastName;
        private int age;
        private String addressCity;
        private String spouse;

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        public String getAddressCity() {
            return addressCity;
        }

        public void setAddressCity(String addressCity) {
            this.addressCity = addressCity;
        }

        public String getSpouse() {
            return spouse;
        }

        public void setSpouse(String spouse) {
            this.spouse = spouse;
        }

        @Override
        public String toString() {
            return "PersonDTO{" +
                    "firstName='" + firstName + '\'' +
                    ", lastName='" + lastName + '\'' +
                    ", age=" + age +
                    ", addressCity='" + addressCity + '\'' +
                    ", spouse='" + spouse + '\'' +
                    '}';
        }
    }

}
