package grp1;

import grp1.repository.SampleRepository;
import org.resthub.common.util.PostInitialize;

import javax.inject.Inject;
import javax.inject.Named;

@Named("sampleInitializer")
public class SampleInitializer {

    @Inject
    @Named("sampleRepository")
    private SampleRepository sampleRepository;

    @PostInitialize
    public void init() {
//        sampleRepository.save(new Sample("testSample1"));
//        sampleRepository.save(new Sample("testSample2"));
//        sampleRepository.save(new Sample("testSample3"));
//        sampleRepository.save(new Sample("testSample4"));
//        sampleRepository.save(new Sample("testSample5"));
//        sampleRepository.save(new Sample(new ConfigurationBean().testValue));
    }
}
